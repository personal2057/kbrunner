const canvas = document.querySelector('canvas')
const c = canvas.getContext('2d')

canvas.width = 1125
canvas.height = 750

const player_size = 75
const matrix_size_x = 14
const matrix_size_y = 9

const matrix_middle_x = Math.round(matrix_size_x / 2)
const matrix_middle_y = Math.round(matrix_size_y / 2)

const LOCATION_FREE = 0
const LOCATION_WALL = 1
const LOCATION_GARBAGE = 2

const enemy_speed = 10

const kb_y = 0
const kb_x = 14

const home_y = 9
const home_x = 0

characters = []

/* 
	0 - ��������� ����
	1 - �����
	2 - ��� ������
*/

const field_matrix = [[1,1,0,0,0,0,1,1,0,0,0,0,0,0,0],
					  [1,1,0,1,1,0,1,1,0,1,0,1,0,0,0],
					  [0,0,0,1,1,0,0,1,0,0,0,0,0,1,1],
					  [0,0,0,0,0,0,0,0,0,0,1,1,0,1,1],
					  [1,0,1,1,0,1,0,0,0,0,1,1,0,1,0],
					  [1,0,1,1,0,1,0,0,0,0,0,0,0,0,0],
					  [0,0,0,0,0,0,0,0,0,1,0,1,1,0,1],
					  [0,0,2,1,1,0,1,0,0,0,0,1,1,0,0],
					  [0,1,0,1,1,0,0,0,1,1,1,1,1,1,0],
					  [0,0,0,0,0,0,1,0,0,0,0,0,0,0,0],]

const start_position_x = 0
const start_position_y = matrix_size_y 
					
c.fillRect(0, 0, canvas.width, canvas.height)



class Sprite{
	constructor({ position, imageSrc }){
		this.position = position
		this.image = new Image()
		this.image.src = imageSrc
	}
	
	draw(){
		c.drawImage(this.image, this.position.x, this.position.y)
	}
	
	update(){
		this.draw()
	}
}



const background = new Sprite({
	position:{
		x: 0,
		y: 0
	},
	imageSrc: './Images/background.png'
})


const garbage = new Sprite({
	position:{
		x: 2 * player_size,
		y: 7 * player_size
	},
	cursor:{
		x: 2,
		y: 7
	},
	imageSrc: './Images/gurbage.png'
})


class Character {
	constructor({ position, velocity, cursor }){
		this.position = position
		this.velocity = velocity
		this.cursor = cursor
		this.visited = false
		this.score = 0
	}
	
	draw(){
		c.fillStyle = 'green'
		//c.fillRect(this.position.x, this.position.y, player_size, player_size)
		c.fillRect(this.cursor.x * player_size, this.cursor.y * player_size, player_size, player_size)
	}
	
	update(){
		this.draw()

		for (let en = 0; en < characters.length; en++){
			if ((characters[en].cursor.x === this.cursor.x) && (characters[en].cursor.y === this.cursor.y)){
				console.log('POPALSYA!!!!!!!!!!!!!!!')
			}
		}
	}
	
	
	//�������� ����, ��� ���������� ������ ����� � ��� �� �����
	checkRange(y, x){
		if ((x >= 0) && (x <= matrix_size_x) &&
			(y >= 0) && (y <= matrix_size_y) &&
			field_matrix[y][x] != LOCATION_WALL){
				return true
		}			
		return false
	}
	
	//��� � ����� �� ������
	makeStep({direction}){		
	
		let can_move = false
	
		//�� �� � ����?
		if (this.checkRange(direction.y, direction.x)){
			//��� ��� ������?
			if (field_matrix[direction.y][direction.x] == LOCATION_GARBAGE){
				//����������� ����������, ���� ��� ���������
				let x1 = (direction.x - this.cursor.x) 
				let y1 = (direction.y - this.cursor.y) 
				//������� ���, ���� ���� ����
				if (this.checkRange(direction.y + y1, direction.x + x1)){
					field_matrix[direction.y + y1][direction.x + x1] = LOCATION_GARBAGE
					field_matrix[direction.y][direction.x] = LOCATION_FREE					
					can_move = true
				}
			} else {
				can_move = true
			}
		}	
		//�����
		if (can_move){				
			this.cursor.x = direction.x
			this.cursor.y = direction.y
			
			
			//���� �� � �� - ����� �����
			if ((this.cursor.x === kb_x) && (this.cursor.y === kb_y)){
				this.visited = true
				console.log('ZATARILSYA')
			}
			
			//���� �� � ������� � ���� - �������� ����
			if ((this.cursor.x === home_x) && (this.cursor.y === home_y) && (this.visited === true)){
				this.score = this.score + 1
				this.visited = false
				console.log(this.score)
			}
		}
	}
	
	
	moveLeft(){
		this.makeStep({direction: {y: this.cursor.y, x: this.cursor.x - 1}})
	}
	
	moveRight(){
		this.makeStep({direction: {y: this.cursor.y, x: this.cursor.x + 1}})
	}
	
	moveUp(){
		this.makeStep({direction: {y: this.cursor.y - 1, x: this.cursor.x}})
	}
	
	moveDown(){
		this.makeStep({direction: {y: this.cursor.y + 1, x: this.cursor.x}})
	}
}




class EnemyCharacter extends Character{	
	
	constructor({ position, velocity, cursor }){
		super({ position, velocity, cursor })
		this.timer = 0
		this.route = []
	}
	
	
	//�������� ����, ��� ���������� ������ ����� � ��� �� �����
	checkRange(y, x){
		if ((x >= 0) && (x <= matrix_size_x) &&
			(y >= 0) && (y <= matrix_size_y) &&
			(field_matrix[y][x] != LOCATION_WALL) &&
			(field_matrix[y][x] != LOCATION_GARBAGE)){
				return true
		}			
		return false
	}
	

	getRandom(min, max) {
		return Math.round(Math.random() * (max - min) + min);
	}
	
	getPoint(){
		//������� ����� ��� ������������
		let field = this.getRandom(0,4)
		let field_x = 0
		let field_y = 0
		
		//������� �������� ������� ����, � ������� ����� ������ �����
		switch (field){
			case 0:
				field_x = 0
				field_y = 0
			break
			case 1:
				field_x = matrix_middle_x
				field_y = 0
			break
			case 2:
				field_x = 0
				field_y = matrix_middle_y
			break
			case 3:
				field_x = matrix_middle_x
				field_y = matrix_middle_y
			break
		}
		
		
		let waypoint_x = 0
		let waypoint_y = 0
		
		//�������� ���� ��������� ���� � ��������� ��������
		do {
			waypoint_x = this.getRandom(field_x, field_x + matrix_middle_x - 1)
			waypoint_y = this.getRandom(field_y, field_y + matrix_middle_y - 1)			
		} 
		while (field_matrix[waypoint_y][waypoint_x] != LOCATION_FREE);
		
		return ({y: waypoint_y, x: waypoint_x})
	}
	
	
	
	setWay(){
		//if (!this.has_point){
			//this.has_point = true
			
			let points = this.getPoint()
			
			let point_x = points.x
			let point_y = points.y
			
			console.log(this.cursor.y, this.cursor.x)
			console.log(point_y, point_x)
			console.log("--------")
		
			let borders = []
			let visited = [] 
			let cur 
			let str_key = ""
			let came_from = new Map()
			
			borders.push({y: this.cursor.y, x: this.cursor.x})
			visited.push([this.cursor.y, this.cursor.x])
			
			str_key = this.cursor.y + "_" + this.cursor.x
			came_from.set(str_key, 0)
	
			while (borders.length > 0){
	
				cur = borders.pop()
				

				if (this.checkRange(cur.y + 1, cur.x) && (JSON.stringify(visited).includes([cur.y  + 1, cur.x]) == false)){
					borders.push({y: cur.y + 1, x: cur.x})
					visited.push([cur.y + 1, cur.x])	
					str_key = (cur.y + 1) + "_" + cur.x
					came_from.set(str_key, {y: cur.y, x: cur.x})	
				} 
				
				if (this.checkRange(cur.y, cur.x + 1) && (JSON.stringify(visited).includes([cur.y, cur.x + 1]) == false)){
					borders.push({y: cur.y, x: cur.x + 1})+
					visited.push([cur.y, cur.x + 1])	
					str_key = cur.y + "_" + (cur.x + 1)
					came_from.set(str_key, {y: cur.y, x: cur.x})
				} 
				
				if (this.checkRange(cur.y - 1, cur.x) && (JSON.stringify(visited).includes([cur.y  - 1, cur.x]) == false)){
					borders.push({y: cur.y - 1, x: cur.x})
					visited.push([cur.y - 1, cur.x])	
					str_key = (cur.y - 1) + "_" + cur.x
					came_from.set(str_key, {y: cur.y, x: cur.x})
				} 
				
				if (this.checkRange(cur.y, cur.x - 1) && (JSON.stringify(visited).includes([cur.y, cur.x - 1]) == false)){
					borders.push({y: cur.y, x: cur.x - 1})
					visited.push([cur.y, cur.x - 1])	
					str_key = cur.y + "_" + (cur.x - 1)
					came_from.set(str_key, {y: cur.y, x: cur.x})
				} 
				
			}

				let path = []
				cur = {y: point_y, x: point_x} 
				path.push(cur)
				
				while (cur != 0){
					str_key = cur.y + "_" + cur.x
					cur = came_from.get(str_key)
					path.push(cur)
				}
				
				return path
				
	
			
		//}
	}
	

	
	
	update(){
		this.timer = this.timer + 1
		let cur
		
		if (this.timer == 2){
			this.route = this.setWay()
		}
		

		
		if (this.timer == 25){
			if (this.route.length > 0){
				cur = this.route.pop()

				if ((cur) && (field_matrix[cur.y][cur.x] == 2)){
					this.route = this.setWay()
				} 
				else {
					this.cursor.y = cur.y 
					this.cursor.x = cur.x 
				}
				
			}
			else {
				this.route = this.setWay()
			}
			this.timer = 3

		}
		
		this.draw()
	}
	
}



const player = new Character({
	position: {
		x: 0,
		y: canvas.height - player_size
	},
	velocity: {
		x: 0,
		y: 0
	},
	cursor: {
		x: 0,
		y: 9
	}
})




const enemy = new EnemyCharacter({
	position: {
		x: canvas.width - player_size,
		y: 0
	},
	velocity: {
		x: 0,
		y: 0
	},
	cursor: {
		x: 1,
		y: 3
	}
})


const enemy2 = new EnemyCharacter({
	position: {
		x: canvas.width - player_size,
		y: 0
	},
	velocity: {
		x: 0,
		y: 0
	},
	cursor: {
		x: 9,
		y: 2
	}
})


characters.push(enemy)
characters.push(enemy2)


function animate(){
	window.requestAnimationFrame(animate)
	c.fillStyle = 'black'
	c.fillRect(0, 0, canvas.width, canvas.height)
	c.fillStyle = 'red'
	
	/*
	for(let i = 0; i < field_matrix.length; i++) {
		for(let j = 0; j < field_matrix[i].length; j++) {
			if (field_matrix[i][j] == 1){
				c.fillStyle = 'grey'
				c.fillRect(j * player_size, i * player_size, player_size, player_size)
			}
			
			if (field_matrix[i][j] == 2){
				c.fillStyle = 'blue'
				c.fillRect(j * player_size, i * player_size, player_size, player_size)				
			}
    }
}*/
	background.update()
	garbage.update()
	player.update()
	enemy.update()
	enemy2.update()

	
	//enemy2.update()
}


animate()

window.addEventListener('keydown', (event) => {
	switch(event.key){
		case 'w':
			player.moveUp()
		break
		case 's':
			player.moveDown()
		break
		case 'a':
			player.moveLeft()
		break
		case 'd':
			player.moveRight()
		break
		case 'ArrowUp':
			player.moveUp()
		break
		case 'ArrowDown':
			player.moveDown()
		break
		case 'ArrowLeft':
			player.moveLeft()
		break
		case 'ArrowRight':
			player.moveRight()
		break
	}
	
})


