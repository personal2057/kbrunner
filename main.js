﻿const canvas = document.querySelector('canvas')
const c = canvas.getContext('2d')

canvas.width = 1125
canvas.height = 750

const player_size = 75
const matrix_size_x = 14
const matrix_size_y = 9

const matrix_middle_x = Math.round(matrix_size_x / 2)
const matrix_middle_y = Math.round(matrix_size_y / 2)

const LOCATION_FREE = 0
const LOCATION_WALL = 1
const LOCATION_GARBAGE = 2

//позиция кэбэшки
const kb_y = 0
const kb_x = 14

//позиция дома
const home_y = 9
const home_x = 0

	
//когда наступает ночь и ее окончание (секунды)
const night_time_start = 45
const night_time_end = 60

var audio = new Audio('./Sounds/music.mp3')
var audio_whip = new Audio('./Sounds/whip.mp3')
var audio_church = new Audio('./Sounds/church.mp3')
var audio_drag = new Audio('./Sounds/drag.mp3')
var audio_morning = new Audio('./Sounds/morning.mp3')
audio_morning.volume = 0.7
var audio_cash = new Audio('./Sounds/cash.mp3')
audio_cash.volume = 0.5

characters = []

/* 
	0 - свободное поле
	1 - стена
	2 - бак мусора  --устарело
*/

const field_matrix = [[1,1,0,0,0,0,1,1,0,0,0,0,0,0,0],
					  [1,1,0,1,1,0,1,1,0,1,0,1,0,0,0],
					  [0,0,0,1,1,0,0,1,0,0,0,0,0,1,1],
					  [0,0,0,0,0,0,0,0,0,0,1,1,0,1,1],
					  [1,0,1,1,0,1,0,0,0,0,1,1,0,1,0],
					  [1,0,1,1,0,1,0,0,0,0,0,0,0,0,0],
					  [0,0,0,0,0,0,0,0,0,1,0,1,1,0,1],
					  [0,0,0,1,1,0,1,0,0,0,0,1,1,0,0],
					  [0,1,0,1,1,0,0,0,1,1,1,1,1,1,0],
					  [0,0,0,0,0,0,1,0,0,0,0,0,0,0,0],]

const start_position_x = 0
const start_position_y = matrix_size_y 
					
c.fillRect(0, 0, canvas.width, canvas.height)


/*
	0 - ожидаем
	1 - играем
	2 - ночь
	3 - проигрышь
*/

class Game{
	static status = 0
	static enemy_speed = 20
	static isNight = false
	
	static saveresults(score, nights){
		this.score = score
		this.nights = nights
	}
	
	static setStatus(code){
		
		//старт
		if ((code === 1) && (this.status === 0)){
			document.querySelector('#startscreen').style.display = "none";
			document.querySelector('#score').style.display = "inline";
			this.status = code
			increaseTimer()
		} 
		//после ночи
		else if ((code === 1) && (this.status === 2)){
			audio_morning.play()
			this.isNight = false
			this.enemy_speed = 20
			player.addScore(0, 1)
			background.changeImage('./Images/background.png')		
			trees.changeImage('./Images/tree.png')
			this.status = code
		} 
		//ночь
		else if (code === 2){
			this.isNight = true
			this.enemy_speed = 10
			background.changeImage('./Images/backgroundnight.png')		
			trees.changeImage('./Images/treenight.png')
			this.status = code
			audio_church.play()
		}	
		//проигрышь
		else if (code === 3){
			document.querySelector('#score').style.display = "none";
			document.querySelector('#finalscreen').style.display = "inline";
			document.querySelector('#results').innerHTML = ' Счет: ' + this.score + ' <br> Ночей: ' + this.nights + ' ';
			this.status = code
		}	
	}
	
	static getEnemySpeed(){
		return this.enemy_speed
	}
	
	
	
	
}

let elapsed_time = 0

function increaseTimer(){
	
	if (Game.status != 3) {
		setTimeout(increaseTimer, 1000)
	}
		
	elapsed_time++
		
	if (elapsed_time % night_time_start === 0){
		Game.setStatus(2)
	} 
	
	if (elapsed_time % night_time_end === 0){
		Game.setStatus(1)
	} 
		
}


class Sprite{
	constructor({ imageSrc, cursor }){
		this.cursor = cursor
		this.image = new Image()
		this.image.src = imageSrc
	}
	
	draw(){
		//если рисуем фон
		if ((this.cursor.x === 0) && (this.cursor.y === 0)){
			c.drawImage(this.image, this.cursor.x, this.cursor.y)
		} else {
			c.drawImage(this.image, this.cursor.x * player_size, this.cursor.y * player_size)
		}
	}
	
	update(){
		this.draw()
	}
	
	changeImage(newImageSrc){
		this.image.src = newImageSrc
	}
}



const background = new Sprite({
	cursor:{
		x: 0,
		y: 0
	},
	imageSrc: './Images/background.png'
})


const garbage = new Sprite({
	cursor:{
		x: 2,
		y: 7
	},
	imageSrc: './Images/gurbage.png'
})

const trees = new Sprite({
	cursor:{
		x: 7,
		y: 4
	},
	imageSrc: './Images/tree.png'
})

class Character {
	constructor({ cursor, imageSrc, framesMax}){
		this.cursor = cursor
		this.visited = false
		this.score = 0
		this.nights = 0
		
		this.image = new Image()
		this.image.src = imageSrc
		this.framesMax = framesMax
		this.framesCurrent = 0
		this.framesElapsed = 0
		this.framesHold = 7
	}
	
	draw(){

		c.drawImage(
			this.image, 
			this.framesCurrent * player_size, 
			0, 
			player_size, 
			player_size, 
			this.cursor.x * player_size, 
			this.cursor.y * player_size, 
			player_size, player_size
		)
	}
	
	update(){
		this.draw()
		this.framesElapsed++
		
		if (this.framesElapsed % this.framesHold === 0){		
			if (this.framesCurrent < this.framesMax - 1) {
				this.framesCurrent++
			} else {
				this.framesCurrent = 0
			}
			
			this.framesElapsed = 0
		}


		for (let en = 0; en < characters.length; en++){
			if ((characters[en].cursor.x === this.cursor.x) && (characters[en].cursor.y === this.cursor.y)){
				Game.saveresults(this.score, this.nights)
				Game.setStatus(3)
			}
		}
	}
	
	setVisited(){
		if (!this.visited){
			audio_cash.play()
		}
		this.visited = true
		this.image.src = './Images/maincharacterbag.png'	
	}
	
	setUnVisited(){
		if (this.visited == true){
			this.visited = false
			this.image.src = './Images/maincharacter.png'
			audio_whip.play()
			this.addScore(1, 0)
		}
	}
	
	addScore(score, nights){
		this.score = this.score + score
		this.nights = this.nights + nights
		document.querySelector('#score').innerHTML = 'Счет: ' + this.score + '<br/>Ночей: ' + this.nights;
	}
	
	
	//проверка того, что координаты внутри карты и там не стена
	checkRange(y, x){
		if ((x >= 0) && (x <= matrix_size_x) &&
			(y >= 0) && (y <= matrix_size_y) &&
			field_matrix[y][x] != LOCATION_WALL){
				return true
		}			
		return false
	}
	
	//там, куда двигаем бак мусора, нет врагов
	checkRangeForGarbage(y,x){
		if (characters){
			for (let en = 0; en < characters.length; en++){
				if ((y == characters[en].cursor.y) && (x == characters[en].cursor.x)){
					return false
				}
			}
		}
		return true
	}
	
	//шаг в любую из сторон
	makeStep({direction}){		
	
		let can_move = false
	
		//мы не у края?
		if (this.checkRange(direction.y, direction.x)){
			//там бак мусора?
			if ((direction.y == garbage.cursor.y) && (direction.x == garbage.cursor.x)){
				//высчитываем координаты, куда бак двигается
				let x1 = (direction.x - this.cursor.x) 
				let y1 = (direction.y - this.cursor.y) 
				//двигаем бак, если есть куда
				if ((this.checkRange(direction.y + y1, direction.x + x1)) && (this.checkRangeForGarbage(direction.y + y1, direction.x + x1))){
					audio_drag.play()
					garbage.cursor.y = direction.y + y1
					garbage.cursor.x = direction.x + x1
					can_move = true	
				}
			} else {
				can_move = true
			}
		}	
		//ходим
		if (can_move){				
			this.cursor.x = direction.x
			this.cursor.y = direction.y
			
			
			//если мы в кб - берем пакет
			if ((this.cursor.x === kb_x) && (this.cursor.y === kb_y) && !(Game.isNight)){
				this.setVisited()
			}
			
			//если мы с пакетом у дома - получаем очко
			if ((this.cursor.x === home_x) && (this.cursor.y === home_y) && (this.visited)){
				this.setUnVisited()
			}
		}
	}
	
	
	moveLeft(){
		this.makeStep({direction: {y: this.cursor.y, x: this.cursor.x - 1}})
	}
	
	moveRight(){
		this.makeStep({direction: {y: this.cursor.y, x: this.cursor.x + 1}})
	}
	
	moveUp(){
		this.makeStep({direction: {y: this.cursor.y - 1, x: this.cursor.x}})
	}
	
	moveDown(){
		this.makeStep({direction: {y: this.cursor.y + 1, x: this.cursor.x}})
	}
}




class EnemyCharacter extends Character{	
	
	constructor({ cursor, imageSrc, framesMax}){
		super({ cursor, imageSrc, framesMax})
		this.route = []
		this.timer = 0
		
		this.image = new Image()
		this.image.src = imageSrc
		this.framesMax = framesMax
		this.framesElapsed = 0
		
		this.free_points = []
		
		for (let i = 0; i <= matrix_size_x; i++){
			for (let j = 0; j < matrix_size_y; j++){
				if (field_matrix[j][i] === 0){
					this.free_points.push({x: j,y: i})
				}
			}
		}

	}
	
	//проверка нахождения мусорного бака
	isGarbagePosition(y, x){
		if ((x == garbage.cursor.x) && (y == garbage.cursor.y)){
			return true
		} else {
			return false
		}
	}
	
	
	
	//проверка того, что координаты внутри карты и там не стена
	checkRange(y, x){
		if ((x >= 0) && (x <= matrix_size_x) &&
			(y >= 0) && (y <= matrix_size_y) &&
			(field_matrix[y][x] != LOCATION_WALL) &&
			(!this.isGarbagePosition(y, x))){
				return true
		}			
		return false
	}
	

	getRandom(min, max) {
		return Math.round(Math.random() * (max - min) + min);
	}
	

	getPoint(){
		//находим точку для передвижения
		/*let field = this.getRandom(0,4)
		let field_x = 0
		let field_y = 0
		
		//сначала выбираем квадрат поля, в котором будем искать точку
		switch (field){
			case 0:
				field_x = 0
				field_y = 0
			break
			case 1:
				field_x = matrix_middle_x
				field_y = 0
			break
			case 2:
				field_x = 0
				field_y = matrix_middle_y
			break
			case 3:
				field_x = matrix_middle_x
				field_y = matrix_middle_y
			break
		}
		
		
		let waypoint_x = 0
		let waypoint_y = 0
		
		//рандомно ищем свободное поле в выбранном квадрате
		do {
			waypoint_x = this.getRandom(field_x, field_x + matrix_middle_x - 1)
			waypoint_y = this.getRandom(field_y, field_y + matrix_middle_y - 1)		
		} 
		while (field_matrix[waypoint_y][waypoint_x] != LOCATION_FREE);
		*/
		
		let waypoint = this.free_points[this.getRandom(0, this.free_points.length - 1)]
		let waypoint_x = waypoint.y
		let waypoint_y = waypoint.x
		
		return ({y: waypoint_y, x: waypoint_x})
	}
	
	
	
	setWay(){
		//if (!this.has_point){
			//this.has_point = true
			
			let points = this.getPoint()
			
			let point_x = points.x
			let point_y = points.y

		
			let borders = []
			let visited = [] 
			let cur 
			let str_key = ""
			let came_from = new Map()
			
			borders.push({y: this.cursor.y, x: this.cursor.x})
			visited.push([this.cursor.y, this.cursor.x])
			
			str_key = this.cursor.y + "_" + this.cursor.x
			came_from.set(str_key, 0)
			
			
			
			while (borders.length > 0){
	
				cur = borders.pop()
				
				if ((cur.x == point_x) && (cur.y == point_y)){
					break
				} 

				if (this.checkRange(cur.y + 1, cur.x) && (JSON.stringify(visited).includes([cur.y  + 1, cur.x]) == false)){
					borders.push({y: cur.y + 1, x: cur.x})
					visited.push([cur.y + 1, cur.x])	
					str_key = (cur.y + 1) + "_" + cur.x
					came_from.set(str_key, {y: cur.y, x: cur.x})	
				} 
				
				if (this.checkRange(cur.y, cur.x + 1) && (JSON.stringify(visited).includes([cur.y, cur.x + 1]) == false)){
					borders.push({y: cur.y, x: cur.x + 1})+
					visited.push([cur.y, cur.x + 1])	
					str_key = cur.y + "_" + (cur.x + 1)
					came_from.set(str_key, {y: cur.y, x: cur.x})
				} 
				
				if (this.checkRange(cur.y - 1, cur.x) && (JSON.stringify(visited).includes([cur.y  - 1, cur.x]) == false)){
					borders.push({y: cur.y - 1, x: cur.x})
					visited.push([cur.y - 1, cur.x])	
					str_key = (cur.y - 1) + "_" + cur.x
					came_from.set(str_key, {y: cur.y, x: cur.x})
				} 
				
				if (this.checkRange(cur.y, cur.x - 1) && (JSON.stringify(visited).includes([cur.y, cur.x - 1]) == false)){
					borders.push({y: cur.y, x: cur.x - 1})
					visited.push([cur.y, cur.x - 1])	
					str_key = cur.y + "_" + (cur.x - 1)
					came_from.set(str_key, {y: cur.y, x: cur.x})
				} 
				
			}

				let path = []
				cur = {y: point_y, x: point_x} 
				path.push(cur)
				
				while ((cur != 0) || (came_from.length > 0)){
					str_key = cur.y + "_" + cur.x
					cur = came_from.get(str_key)
					came_from.delete(str_key)
					path.push(cur)					
				}
				return path
				
	
			
		//}
	}
	
	update(){
		
		if (this.timer == 0){
			this.route = this.setWay()
			
		}
		
		this.timer++
		let cur
		
		if (this.timer % Game.getEnemySpeed() === 0){
			if (this.route.length > 0){
				cur = this.route.pop()

				if ((cur) && (this.isGarbagePosition(cur.y, cur.x))){
					this.route = this.setWay()
				} 
				else {
					if ((typeof cur.y !== 'undefined') || ((typeof cur.x !== 'undefined'))){
						this.cursor.y = cur.y 
						this.cursor.x = cur.x 
					}
				}
			}
			else {
				this.route = this.setWay()
			}
		}
		
		

		this.framesElapsed++
		
		if (this.framesElapsed % this.framesHold === 0){		
			if (this.framesCurrent < this.framesMax - 1) {
				this.framesCurrent++
			} else {
				this.framesCurrent = 0
			}
			
			this.framesElapsed = 0
		}
		
		this.draw()
		
		
	}
	
}



const player = new Character({
	cursor: {
		x: 0,
		y: 9
	},
	imageSrc: './Images/maincharacter.png',
	framesMax: 5
})




const enemy = new EnemyCharacter({
	cursor: {
		x: 1,
		y: 3
	},
	imageSrc: './Images/enemy.png',
	framesMax: 5
})


const enemy2 = new EnemyCharacter({
	cursor: {
		x: 9,
		y: 2
	},
	imageSrc: './Images/enemy.png',
	framesMax: 5
})


const enemy3 = new EnemyCharacter({
	cursor: {
		x: 13,
		y: 5
	},
	imageSrc: './Images/enemy.png',
	framesMax: 5
})




characters.push(enemy)
characters.push(enemy2)
characters.push(enemy3)

function animate(){
	
	window.requestAnimationFrame(animate)
	
	
	if ((Game.status === 0) || (Game.status === 3)){
		background.update()
	} else {
		background.update()
		garbage.update()
		player.update()
		
		for (let i = 0; i < characters.length; i++){
			characters[i].update()
		}
		trees.update()
	}
	
}


animate()

window.addEventListener('keydown', (event) => {
	switch(event.key){
		case 'ArrowUp':
		case 'w':
			player.moveUp()
		break
		case 'ArrowDown':
		case 's':
			player.moveDown()
		break
		case 'ArrowLeft':
		case 'a':
			player.moveLeft()
		break
		case 'ArrowRight':
		case 'd':
			player.moveRight()
		break
		case ' ':
		case 'Space':
			Game.setStatus(1)
			audio.volume = 0.3
			audio.play()
		break
	}
	
})


